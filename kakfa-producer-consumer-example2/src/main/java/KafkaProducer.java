import java.util.Properties;
import kafka.javaapi.producer.Producer;
import kafka.producer.KeyedMessage;
import kafka.producer.ProducerConfig;

public class KafkaProducer {
	public static void main(String arg[]) {

		String METADATA_BROKER_LIST = "metadata.broker.list";
		String ACKS_REQUIRED = "request.required.acks";
		String ACKS_DEFAULT = "1";
		String SERIALIZER_CLASS = "serializer.class";
		String KAFKA_ENCODER = "kafka.serializer.StringEncoder";
		String REQUEST_TIMEOUT = "request.timeout.ms";
		String PRODUCER_TYPE = "producer.type";
		String MSG_SEND_RETRY_COUNT = "message.send.max.retries";
		Properties producerConfigProps = new Properties();
		producerConfigProps.setProperty(METADATA_BROKER_LIST, "kafka-cisdevpln.int.thomsonreuters.com:9092");
		producerConfigProps.setProperty(ACKS_REQUIRED, ACKS_DEFAULT);
		producerConfigProps.setProperty(SERIALIZER_CLASS, KAFKA_ENCODER);
		ProducerConfig config = new ProducerConfig(producerConfigProps);
		Producer<String, String> producer = new Producer<String, String>(config);
		System.out.println("test1");
		KeyedMessage<String,String> keyedMessage = new KeyedMessage<String,String>("ecp.environment.cem.dev.control.test","Testing for DEMO Learning");
		producer.send(keyedMessage);
	      System.out.println("Send message successfully");
		producer.close();

	}

}
